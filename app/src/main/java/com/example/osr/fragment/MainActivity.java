package com.example.osr.fragment;
import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {
    Button b1,b2;

String l="kansal";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1=(Button)findViewById(R.id.bt1);
        b2=(Button)findViewById(R.id.bt2);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft= fragmentManager.beginTransaction();
                fragmentOne f1=new fragmentOne();
                Bundle b= new Bundle();
                b.putString("arpit", l);
                f1.setArguments(b);
                ft.replace(R.id.fr1_id,f1);
                ft.commit();


            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft= fragmentManager.beginTransaction();
                FragmentTwo f2=new FragmentTwo();
                ft.replace(R.id.fr1_id,f2);
                ft.commit();

            }
        });
    }
}
